import './App.css';
import CatPage from './Pages/CatPage';
import MemePage from './Pages/MemePage';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navbar from './Components/Navbar';
import DogPage from './Pages/DogPage';
import CatProvider from './Contexts/CatProvider';
import CounterPage from './Pages/CounterPage';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <CatProvider>
          <Routes>
            <Route path='/' element={<CatPage />} />
            <Route path='/cats' element={<CatPage />} />
            <Route path='/cats/:catId' element={<CatPage />} />
            <Route path='/memes' element={<MemePage />} />
            <Route path='/dogs' element={<DogPage />} />
            <Route path='/counter' element={<CounterPage />} />
            <Route path='*' element={
              <>
                <h2>This page does not exist!</h2>
                <img src='https://cdn-images-1.medium.com/max/800/1*qdFdhbR00beEaIKDI_WDCw.gif' alt='lost-ghost' />
              </>
            } />
          </Routes>
        </CatProvider>
      </div>
    </BrowserRouter>
  );
}

export default App;
