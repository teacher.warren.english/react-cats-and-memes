import React, { useEffect, useState } from 'react'
import Dashboard from '../Components/Dashboard'

function DogPage() {
    const [dogImage, setDogImage] = useState(null)

    useEffect(() => {
        fetch("https://dog.ceo/api/breeds/image/random")
            .then(resp => resp.json())
            .then(data => setDogImage(data.message))
            .catch(error => console.error(error.message))
    }, [])

    return (
        <>
            <Dashboard dog={dogImage} />
        </>
    )
}

export default DogPage