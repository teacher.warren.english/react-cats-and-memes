import React from 'react';
import { useParams } from 'react-router-dom';
import Cats from '../Components/Cats';

function CatPage() {

    const {catId} = useParams()

    const number1 = 50

    return (
        <Cats identity={catId} myNum={number1} />
    )
}

export default CatPage