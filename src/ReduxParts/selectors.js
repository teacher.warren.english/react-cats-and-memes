export function getCount(state) {
    return state.count
}

export function getCountPositive(state) {
    return state.count > -1
}