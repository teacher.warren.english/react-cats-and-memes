// Action Labels
export const ACTION_INCREMENT = '[count] INCREMENT'
export const ACTION_DECREMENT = '[count] DECREMENT'
export const ACTION_BOOST = '[count] BOOST'

export const increment = () => ({
    type: ACTION_INCREMENT,
})

export const decrement = () => ({
    type: ACTION_DECREMENT,
})

export const boost = (offset = 1) => ({
    type: ACTION_BOOST,
    payload: offset,
})

export const superboost = () => ({
    type: '[count] SUPERBOOST',
})