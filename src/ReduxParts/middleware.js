import { boost } from "./actions"

export const countMiddleware = ({dispatch}) => next => action => {
    next(action)
    if (action.type === '[count] SUPERBOOST')
        dispatch(boost(1000))
}