import { ACTION_BOOST, ACTION_DECREMENT, ACTION_INCREMENT } from "./actions"

export function countReducer(state = 0, action) {
    switch (action.type) {
        case ACTION_INCREMENT:
            return state + 1
        case ACTION_DECREMENT:
            return state - 1
        case ACTION_BOOST:
            
            return state + action.payload

        default:
            return state
    }
}