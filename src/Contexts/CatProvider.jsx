import React, { createContext, useState } from 'react'

export const CatContext = createContext()

function CatProvider(props) {

const [isCatTouched, setIsCatTouched] = useState(false)

  return (
    <CatContext.Provider value={[isCatTouched, setIsCatTouched]}>
        {props.children}
    </CatContext.Provider>
  )
}

export default CatProvider