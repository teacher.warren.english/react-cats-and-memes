import React from 'react'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { boost, decrement, increment, superboost } from '../ReduxParts/actions'
import { getCount, getCountPositive } from '../ReduxParts/selectors'

function Counting() {

    // Hooks
    const [boostAmount, setBoostAmount] = useState(5)
    const [inputError, setInputError] = useState(false)

    // selectors
    const count = useSelector(getCount)
    const isCountPositive = useSelector(getCountPositive)

    // dispatchers
    const dispatch = useDispatch()
    const handleIncrement = () => dispatch(increment())
    const handleDecrement = () => dispatch(decrement())
    const handleBoost = () => dispatch(boost(parseInt(boostAmount)))
    const handleSuperBoost = () => dispatch(superboost())

    //handler
    const handleBoostAmountChange = (event) => {

        if (isNaN(event.target.value) || event.target.value === '') {
            console.log('ISNAN');
            setInputError(true)
            setBoostAmount(0)
        }
        else {
            console.log('!ISNAN');
            setInputError(false)
            setBoostAmount(event.target.value)
        }
    }

    return (
        <div>
            <h2>Counter: {count}</h2>
            <h3>Count is {isCountPositive ? 'positive' : 'negative'}</h3>

            <button onClick={handleIncrement}>Increase</button>
            <button onClick={handleDecrement}>Decrease</button>
            <div>
                <label htmlFor="boost-amount">Boost amount:</label>
                <input
                    id='boost-amount' onChange={handleBoostAmountChange} type="text" placeholder='5'
                />
                <button disabled={inputError} onClick={handleBoost}>Boost</button>
            </div>
            <button onClick={handleSuperBoost}>SuperBoost</button>

        </div>
    )
}

export default Counting