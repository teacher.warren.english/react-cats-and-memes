import React, { useContext, useState } from 'react'
import { CatContext } from '../Contexts/CatProvider'

function Cats(props) {

  // Hooks
  const [name, setName] = useState('')
  const [isCatTouched, setIsCatTouched] = useContext(CatContext)

  const lecturer = 'Warren'

  const handleCatClicked = () => {
    // do stuff
    // reassign useState name
    console.log('Hey!');
    console.log(props);
    setName('Warren')
    setIsCatTouched(true)
  }

  return (
    <>
      <div>CatPage</div>
      <img onClick={handleCatClicked} src="https://news.wisc.edu/content/uploads/2016/03/cat-959275_960_720.jpg" alt="the-cat" />
      {props.identity && <p>{lecturer}</p>}
      <p>{props.catId}</p>
      <p>{props.myNum}</p>
      {isCatTouched && <p>😻</p>}
    </>
  )
}

export default Cats