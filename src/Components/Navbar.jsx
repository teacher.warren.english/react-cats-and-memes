import React from 'react'
import { NavLink } from 'react-router-dom'

function Navbar() {
  return (
    <nav>
        <li><NavLink to='/cats'>Cats</NavLink></li>
        <li><NavLink to='/memes'>Memes</NavLink></li>
        <li><NavLink to='/dogs'>Dogs</NavLink></li>
        <li><NavLink to='/counter'>Counter</NavLink></li>
    </nav>
  )
}

export default Navbar